-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 13-07-2021 a las 17:30:26
-- Versión del servidor: 5.7.24
-- Versión de PHP: 5.6.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `veikel_db`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `countries`
--

CREATE TABLE `countries` (
  `id` int(20) NOT NULL,
  `country` varchar(250) NOT NULL,
  `shortname` varchar(2) NOT NULL,
  `language` varchar(255) NOT NULL,
  `ContactData` text NOT NULL,
  `webEmailContact` varchar(255) NOT NULL,
  `supportEmailContact` varchar(255) NOT NULL,
  `salesEmailContact` varchar(255) NOT NULL,
  `facebookUrl` varchar(255) NOT NULL,
  `twitterUrl` varchar(255) NOT NULL,
  `instagramUrl` varchar(255) NOT NULL,
  `gplusUrl` varchar(255) NOT NULL,
  `youtubeUrl` varchar(255) NOT NULL,
  `newsletter` int(1) NOT NULL DEFAULT '1',
  `product_register` int(1) NOT NULL DEFAULT '1',
  `service_centers` int(1) NOT NULL DEFAULT '1',
  `employ` int(1) NOT NULL DEFAULT '1',
  `center_service` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `countries`
--

INSERT INTO `countries` (`id`, `country`, `shortname`, `language`, `ContactData`, `webEmailContact`, `supportEmailContact`, `salesEmailContact`, `facebookUrl`, `twitterUrl`, `instagramUrl`, `gplusUrl`, `youtubeUrl`, `newsletter`, `product_register`, `service_centers`, `employ`, `center_service`) VALUES
(1, 'Mexico', 'mx', 'spanish', '', '', '', '', '', '', '', '', '', 1, 1, 1, 1, ''),
(2, 'Argentina', 'ar', 'spanish', '', '', '', '', '', '', '', '', '', 1, 1, 1, 1, ''),
(3, 'Chile', 'cl', 'spanish', '', '', '', '', '', '', '', '', '', 1, 1, 1, 1, ''),
(4, 'International', 'us', 'english', '', '', '', '', '', '', '', '', '', 1, 1, 1, 1, ''),
(5, 'Brasil', 'br', 'portuguese', '', '', '', '', '', '', '', '', '', 1, 1, 1, 1, ''),
(6, 'Colombia', 'co', 'spanish', '', '', '', '', '', '', '', '', '', 1, 1, 1, 1, ''),
(7, 'Puerto Rico', 'pr', 'spanish', '', '', '', '', '', '', '', '', '', 1, 1, 1, 1, ''),
(8, 'Francia', 'fr', 'french', '', '', '', '', '', '', '', '', '', 1, 1, 1, 1, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lang`
--

CREATE TABLE `lang` (
  `id` int(11) UNSIGNED NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `abr` varchar(55) DEFAULT NULL,
  `lang_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `lang`
--

INSERT INTO `lang` (`id`, `titulo`, `abr`, `lang_name`) VALUES
(5, 'Español', 'es', 'spanish'),
(9, 'Portugues', 'pt', 'portuguese'),
(10, 'Ingles', 'en', 'english'),
(11, 'Frances', 'fr', 'french');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `lang`
--
ALTER TABLE `lang`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `lang`
--
ALTER TABLE `lang`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

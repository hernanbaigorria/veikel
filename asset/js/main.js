// Helper function for add element box list in WOW
WOW.prototype.addBox = function(element) {
  this.boxes.push(element);
};

// Init WOW.js and get instance
var wow = new WOW();
wow.init();



// Attach scrollSpy to .wow elements for detect view exit events,
// then reset elements and add again for animation
$('.wow').on('scrollSpy:exit', function() {
  $(this).css({
    'visibility': 'hidden',
    'animation-name': 'none'
  }).removeClass('animated');
  wow.addBox(this);
}).scrollSpy();


$('.box-mundo').on('scrollSpy:enter', function() {//when scrolling and element showing in window
    $('.text-01').addClass('animate-text-01');
    $('.text-02').addClass('animate-text-02');
    $('.globo').addClass('translate-globo');
    $('.aro').addClass('translate-globo');
});

$('.box-mundo').on('scrollSpy:exit', function() {//when scrolling and element exit at window
    $('.text-01').removeClass('animate-text-01');
    $('.text-02').removeClass('animate-text-02');
    $('.globo').removeClass('translate-globo');
    $('.aro').removeClass('translate-globo');
});

$('.box-mundo').scrollSpy();


$('.pastilla_producto').on('scrollSpy:enter', function() {//when scrolling and element showing in window
    $('.absolute-img-pastilla').addClass('transitiom-img-pastilla');
});

$('.pastilla_producto').on('scrollSpy:exit', function() {//when scrolling and element exit at window
    $('.absolute-img-pastilla').removeClass('transitiom-img-pastilla');
});

$('.pastilla_producto').scrollSpy();


$('.circle_middle_2').on('scrollSpy:enter', function() {//when scrolling and element showing in window
    $('.absolute-circle-estudiantes').addClass('transitiom-img-pastilla-estudiantes');
});

$('.circle_middle_2').on('scrollSpy:exit', function() {//when scrolling and element exit at window
    $('.absolute-circle-estudiantes').removeClass('transitiom-img-pastilla-estudiantes');
});

$('.circle_middle_2').scrollSpy();

$('.slide-estudiantes-pastillas').on('scrollSpy:enter', function() {//when scrolling and element showing in window
    $('.absolute-circle-estudiantes-02').addClass('transitiom-img-pastilla-estudiantes');
});

$('.slide-estudiantes-pastillas').on('scrollSpy:exit', function() {//when scrolling and element exit at window
    $('.absolute-circle-estudiantes-02').removeClass('transitiom-img-pastilla-estudiantes');
});

$('.slide-estudiantes-pastillas').scrollSpy();


$('.animacion-imagen-presion').on('scrollSpy:enter', function() {//when scrolling and element showing in window
    $('.detalle-presion').addClass('detalle-presion-transition');
});

$('.animacion-imagen-presion').on('scrollSpy:exit', function() {//when scrolling and element exit at window
    $('.detalle-presion').removeClass('detalle-presion-transition');
});

$('.animacion-imagen-presion').scrollSpy();

    //create var for points

    //This adds the hover function to change to blue on mouseover then back to green on mouseout
    $( "#animate-link-01" ).hover(
      function() {
        $( '.animate-img' ).addClass('transition');
        $( '.rotate' ).addClass('transition-rotate');
        $( '.animate-p' ).addClass('transition-p');
        
     

        }, function() {
        $( '.animate-img' ).removeClass('transition');
        $( '.rotate' ).removeClass('transition-rotate');
        $( '.animate-p' ).removeClass('transition-p');
      }
    );

    $( "#animate-link-02" ).hover(
      function() {
        $( '.animate-img-02' ).addClass('transition');
      $( '.rotate-02' ).addClass('transition-rotate');
      $( '.animate-p-02' ).addClass('transition-p');

        }, function() {
        $( '.animate-img-02' ).removeClass('transition');
        $( '.rotate-02' ).removeClass('transition-rotate');
        $( '.animate-p-02' ).removeClass('transition-p');
      }
    );


    $( "#animate-link-03" ).hover(
      function() {
      $( '.rotate' ).addClass('transition-rotate');
      $( '.animate-p' ).addClass('transition-p');

        }, function() {
        $( '.rotate' ).removeClass('transition-rotate');
        $( '.animate-p' ).removeClass('transition-p');
      }
    );

    $( "#animate-product-01" ).hover(
      function() {
        $( '.animate-img-03' ).addClass('transition');
        $( '.animate-img-04' ).addClass('transition');
        $( '.rotate' ).addClass('transition-rotate');
        $( '.animate-p' ).addClass('transition-p');
        
     

        }, function() {
        $( '.animate-img-03' ).removeClass('transition');
        $( '.animate-img-04' ).removeClass('transition');
        $( '.rotate' ).removeClass('transition-rotate');
        $( '.animate-p' ).removeClass('transition-p');
      }
    );

    $( "#animate-product-02" ).hover(
      function() {
        $( '.animate-img-03' ).addClass('transition');
        $( '.animate-img-05' ).addClass('transition');
        $( '.rotate-03' ).addClass('transition-rotate');
        $( '.animate-p-03' ).addClass('transition-p');
        
     

        }, function() {
        $( '.animate-img-03' ).removeClass('transition');
        $( '.animate-img-05' ).removeClass('transition');
        $( '.rotate-03' ).removeClass('transition-rotate');
        $( '.animate-p-03' ).removeClass('transition-p');
      }
    );

    $( "#animate-product-03" ).hover(
      function() {
        $( '.animate-img-03' ).addClass('transition');
        $( '.animate-img-06' ).addClass('transition');
        $( '.rotate-04' ).addClass('transition-rotate');
        $( '.animate-p-04' ).addClass('transition-p');
        
     

        }, function() {
        $( '.animate-img-03' ).removeClass('transition');
        $( '.animate-img-06' ).removeClass('transition');
        $( '.rotate-04' ).removeClass('transition-rotate');
        $( '.animate-p-04' ).removeClass('transition-p');
      }
    );

    $( "#animate-product-04" ).hover(
      function() {
        $( '.animate-img-03' ).addClass('transition');
        $( '.animate-img-07' ).addClass('transition');
        $( '.rotate-05' ).addClass('transition-rotate');
        $( '.animate-p-05' ).addClass('transition-p');
        
     

        }, function() {
        $( '.animate-img-03' ).removeClass('transition');
        $( '.animate-img-07' ).removeClass('transition');
        $( '.rotate-05' ).removeClass('transition-rotate');
        $( '.animate-p-05' ).removeClass('transition-p');
      }
    );


$('#slider_middle').owlCarousel({
    loop:true,
    animateOut: 'fadeOut',
    autoplay: true,
    autoplayHoverPause: false,
    margin:0,
    smartSpeed:350,
    nav:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
})


$(window).scroll(function() {    

    var scroll = $(window).scrollTop();

    if (scroll >= 500) {
        $(".flechas").addClass("opacity-1");
    }else {
      $(".flechas").removeClass("opacity-1");
    }
});

$('.content-extra').on('scrollSpy:enter', function() {//when scrolling and element exit at window
  $('.counter-count').each(function () {
      $(this).prop('Counter',0).animate({
          Counter: $(this).text()
      }, {
          duration: 3000,
          easing: 'swing',
          step: function (now) {
              $(this).text(Math.ceil(now));
          }
      });
  });
});

$( ".si_acepto" ).click(function() {
  $( ".modal-mexico" ).slideToggle( "slow" );
});

$( ".no_acepto" ).click(function() {
  $( ".show_error" ).slideToggle( "slow" );
});


$('.content-extra').scrollSpy();

$('.relativeimages').on('scrollSpy:enter', function() {//when scrolling and element showing in window
    $('.aro-2').addClass('transition-aro-02');
});

$('.relativeimages').on('scrollSpy:exit', function() {//when scrolling and element exit at window
    $('.aro-2').removeClass('transition-aro-02');
});

$('.relativeimages').scrollSpy();

/*
$('#slider_humans').owlCarousel({
    loop:true,
    animateOut: 'fadeOut',
    autoplay: true,
    autoplayHoverPause: false,
    margin:0,
    dots: true,
    smartSpeed:450,
    nav:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
})*/


 var action = false, clicked = false;
 var Owl = {

     init: function() {
       Owl.carousel();
     },

  carousel: function() {
    var owl;
    $(document).ready(function() {
      
      owl = $('#slider_humans').owlCarousel({
        items    : 1,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        autoplay: true,
        autoplayHoverPause: false,
        smartSpeed:150,
        fluidSpeed:150,
        nav        : false,
        dots       : true,
        loop       : true,
        margin     : 0,
        dotsContainer: '.nav-humans',
      });

       $('.nav-custom').on('click', 'li', function(e) {
          owl.trigger('to.owl.carousel', [$(this).index(), 300]);
        });
    });
  }
 };

 $(document).ready(function() {
   Owl.init();
 });



  $('#slider_pastilla_estudiantes').owlCarousel({
     loop:true,
     margin:10,
     animateOut: 'fadeOut',
     animateIn: 'fadeIn',
     autoplay: true,
     nav:false,
     //navText: ["<i style='display:none' class='fa fa-chevron-left'></i>","<div class='arrow'><div class='chevron'></div><div class='chevron'></div><div class='chevron'></div></div><p class='animate-p'>Ver Más</p>"],
     responsive:{
         0:{
             items:1
         },
         600:{
             items:1
         },
         1000:{
             items:2
         }
     }
 });

function esVacio (a) {
    if (a == "")
		return true;
	else
		return false;
}

function validarEmail(str) 
{
	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(str))
	{
		return (true);
  	} 
  	else 
  	{
     	return (false);
  	}
}

function parsearJSON(str){
	try{
		return eval('('+str+')');
	}catch(err){
		return null;
	}
}

function JSONtoString(json){
	try{
		return JSON.stringify(json);
	}catch(err){
		return null;
	}
}

function encode(str){
	try{
		return $.base64Encode(str);
	}catch(err){
		return null;
	}
}

function decode(str){
	try{
		if(str.indexOf('}str_end')>-1){
			str=str.substring(0,str.lastIndexOf('}str_end'))
		}
		return $.base64Decode(str);
	}catch(err){
		return null;
	}
}


function utf8_encode (argString) {

    if (argString === null || typeof argString === "undefined") {
        return "";
    }
     var string = (argString + '');
    var utftext = "",
        start, end, stringl = 0;
 
    start = end = 0;    stringl = string.length;
    for (var n = 0; n < stringl; n++) {
        var c1 = string.charCodeAt(n);
        var enc = null;
         if (c1 < 128) {
            end++;
        } else if (c1 > 127 && c1 < 2048) {
            enc = String.fromCharCode((c1 >> 6) | 192) + String.fromCharCode((c1 & 63) | 128);
        } else {            enc = String.fromCharCode((c1 >> 12) | 224) + String.fromCharCode(((c1 >> 6) & 63) | 128) + String.fromCharCode((c1 & 63) | 128);
        }
        if (enc !== null) {
            if (end > start) {
                utftext += string.slice(start, end);            }
            utftext += enc;
            start = end = n + 1;
        }
    } 
    if (end > start) {
        utftext += string.slice(start, stringl);
    }
     return utftext;
}

function utf8_decode (str_data) {
  var tmp_arr = [],
    i = 0,
    ac = 0,
    c1 = 0,
    c2 = 0,
    c3 = 0;

  str_data += '';

  while (i < str_data.length) {
    c1 = str_data.charCodeAt(i);
    if (c1 < 128) {
      tmp_arr[ac++] = String.fromCharCode(c1);
      i++;
    } else if (c1 > 191 && c1 < 224) {
      c2 = str_data.charCodeAt(i + 1);
      tmp_arr[ac++] = String.fromCharCode(((c1 & 31) << 6) | (c2 & 63));
      i += 2;
    } else {
      c2 = str_data.charCodeAt(i + 1);
      c3 = str_data.charCodeAt(i + 2);
      tmp_arr[ac++] = String.fromCharCode(((c1 & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
      i += 3;
    }
  }

  return tmp_arr.join('');
}

function convertToPath(str){
	str=str.replace(/"/g,'_');
	var tofind = "ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ;,%'\"°!$";
	var replac = "AAAAAAaaaaaaOOOOOOooooooEEEEeeeeCcIIIIiiiiUUUUuuuuyNn________";
	
	tofind = tofind.split('');
	replac = replac.split('');
	for(var i=0 ; i < tofind.length ; i++){
		str=str.replace(new RegExp(tofind[i],'g'),replac[i]);
	}
	str = str.toLowerCase().split(".");
	str = str.join("_");
	str = str.toLowerCase().split(" ");
	str = str.join("-");

	return str;
}	
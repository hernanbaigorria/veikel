<section class="seccion-01">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-12 col-md-4" style="position:relative;">
				<h3 class="wow fadeInRight" data-wow-duration="1.1s" data-wow-delay="0.5s"><strong>Bienvenidos a la evolución</strong><br>del comercio de respuestos</h3>
				<img src="<?=base_url()?>asset/img/icon_detalle_verde.png" data-wow-duration="1.1s" data-wow-delay="1s" class="img-fluid absolute-detalle wow fadeInLeft">
			</div>
			<div class="col-12 col-md-1"></div>
			<div class="col-12 col-md-7">
				<img src="<?=base_url()?>asset/img/telefono_herramientas.png" data-wow-duration="1.1s" data-wow-delay="0.5s" class="img-fluid wow fadeIn">
			</div>
		</div>
	</div>
</section>

<section class="seccion-02">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-5" style="margin-top:auto;">
				<img src="<?php echo base_url() ?>asset/img/logo_m_detalle.png" class="img-fluid wow fadeIn" data-wow-duration="1.1s" data-wow-delay="0.5s">
			</div>
			<div class="col-12 col-md-1"></div>
			<div class="col-12 col-md-5">
				<h4 class="wow fadeInUp" data-wow-duration="1.1s" data-wow-delay="0.5s">Presentamos la plataforma pensada específicamente para el e-commerce<br><strong>de todos los repuestos.</strong></h4>
				<p class="wow fadeInUp" data-wow-duration="1.1s" data-wow-delay="0.5s">Directo al consumidor, mercado, gestión de canales, pasillo sin fin. Venda sus repuestos y servicios de la forma que desee con VEIKEL.</p>
				<a href="#" class="btns-ir wow fadeInDown" data-wow-duration="1.1s" data-wow-delay="0.5s">Empieza ahora</a>
			</div>
			<div class="col-12 col-md-1"></div>
		</div>
	</div>
</section>

<section class="seccion-03">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h3 class="wow fadeIn" data-wow-duration="1.1s" data-wow-delay="0.5s"><img src="<?=base_url()?>asset/img/icon_detalle_violeta.png" class="img-fluid wow fadeInLeft" data-wow-duration="1.1s" data-wow-delay="1s"> ¿Qué podés hacer en Veikel?
				</h3>
			</div>
			<div class="col-12 col-md-4 text-center wow fadeInDown" data-wow-duration="1.1s" data-wow-delay="0.5s">
				<img src="<?=base_url()?>asset/img/icon_01.png" class="img-fluid icon-">
				<h4>Lorem Ipsum ecommerce</h4>
				<p>Utilice nuestras capacidades integradas y funciones listas para usar para implementar su operación comercial con el tiempo de comercialización más rápido.</p>
			</div>
			<div class="col-12 col-md-4 text-center wow fadeInDown" data-wow-duration="1.1s" data-wow-delay="1s">
				<img src="<?=base_url()?>asset/img/icon_02.png" class="img-fluid icon-">
				<h4>Lorem Ipsum ecommerce</h4>
				<p>Utilice nuestras capacidades integradas y funciones listas para usar para implementar su operación comercial con el tiempo de comercialización más rápido.</p>
			</div>
			<div class="col-12 col-md-4 text-center wow fadeInDown" data-wow-duration="1.1s" data-wow-delay="1.5s">
				<img src="<?=base_url()?>asset/img/icon_03.png" class="img-fluid icon-">
				<h4>Lorem Ipsum ecommerce</h4>
				<p>Utilice nuestras capacidades integradas y funciones listas para usar para implementar su operación comercial con el tiempo de comercialización más rápido.</p>
			</div>
		</div>
	</div>
</section>
<div id="empezarAhora" style="position:relative;top:-150px;"></div>
<section class="seccion-04">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-12 col-md-6">
				<img src="<?=base_url()?>asset/img/telefono_mano.png" class="img-fluid tel-mano wow fadeIn" data-wow-duration="1.1s" data-wow-delay="0.5s">
			</div>
			<div class="col-12 col-md-5">
				<h3 class="wow fadeInUp" data-wow-duration="1.1s" data-wow-delay="0.5s"><strong>Uniendo todos
				tus canales</strong><br>de venta
				</h3>
				<p class="wow fadeInUp" data-wow-duration="1.1s" data-wow-delay="0.5s">Integre pedidos e inventario en todos los canales de una manera rentable para su negocio mientras optimiza el cumplimiento y la entrega.</p>
				<div class="d-flex align-items-center justify-content-start flex-wrap">
					<img src="<?=base_url()?>asset/img/com_icons.png" class="img-fluid icons-comp wow fadeInRight" data-wow-duration="1.1s" data-wow-delay="0.5s">
					<a href="#" class="btns-ir wow fadeInLeft" data-wow-duration="1.1s" data-wow-delay="0.5s">Empieza ahora</a>
				</div>
			</div>
			<div class="col-12 col-md-1"></div>
		</div>
	</div>
	<div class="container absolute-detalle">
		<div class="row">
			<div class="col-12 col-md-6"></div>
			<div class="col-12 col-md-5 p-0">
				<img src="<?=base_url()?>asset/img/banda_top_violeta.png" class="img-fluid wow fadeInRight" data-wow-duration="1.1s" data-wow-delay="0.5s">
			</div>
			<div class="col-12 col-md-1"></div>
		</div>
	</div>
</section>

<section class="seccion-05">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-12 col-md-8">
				<img src="<?=base_url()?>asset/img/comillas_grandes.png" class="img-fluid comillas-absolute wow fadeInUp" data-wow-duration="1.1s" data-wow-delay="0.5s">
				<h4 class="wow fadeInUp" data-wow-duration="1.1s" data-wow-delay="0.5s">La venta de respuestos
				en Veikel nos impulsó<br>
				<strong>hacia una nuevos
				negocios digitales.</strong> <img src="<?=base_url()?>asset/img/comillas_simples.png" class="img-fluid"></h4>
				<div class="d-flex align-items-center justify-content-end content-logo-desc">
					<img src="<?=base_url()?>asset/img/logo_reno.png" class="img-fluid wow fadeIn" data-wow-duration="1.1s" data-wow-delay="0.5s">
					<h5 class="wow fadeIn" data-wow-duration="1.1s" data-wow-delay="0.5s"><strong>Lisando Marquez</strong><br>Repuestos Renault Marquez</h5>
				</div>
			</div>
			<div class="col-12 col-md-4">
				<img src="<?=base_url()?>asset/img/persona_sec_bot.png" class="img-fluid wow fadeInRight" data-wow-duration="1.1s" data-wow-delay="0.5s">
			</div>
		</div>
	</div>
</section>
<div id="preguntas" style="position:relative;top:-150px;"></div>
<section class="seccion-06">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h3 class="wow fadeInUp" data-wow-duration="1.1s" data-wow-delay="0.5s">¿Necesitás ayuda?</h3>
				<p class="wow fadeInUp" data-wow-duration="1.1s" data-wow-delay="0.5s">Podés visitar nuestra sección de preguntas frecuentes</p>
				<a href="#" class="btns-ir wow fadeInUp" data-wow-duration="1.1s" data-wow-delay="0.5s">Preguntas frecuentes</a>
			</div>
		</div>
	</div>
</section>

<div id="contacto" style="position:relative;top:-100px;"></div>
<section class="seccion-07">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-2"></div>
			<div class="col-12 col-md-8 text-center">
				<h3 class="wow fadeInUp" data-wow-duration="1.1s" data-wow-delay="0.5s">Contactanos ahora</h3>
				<form class="form-send wow fadeInUp" data-wow-duration="1.1s" data-wow-delay="0.5s" action="<?=base_url().$this->config->item('language_abbr')?>/home/send_form/" method="POST">
					<input type="text" name="nombre" placeholder="Nombre" required>
					<input type="email" name="email" placeholder="Email" required>
					<input type="text" name="telefono" placeholder="Teléfono" required>
					<input type="text" name="asunto" placeholder="Asunto" required>
					<textarea name="mensaje" placeholder="Mensaje"></textarea>
					<input type="submit" value="Enviar">
				</form>
			</div>
			<div class="col-12 col-md-2"></div>
		</div>
	</div>
</section>
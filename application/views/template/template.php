<!DOCTYPE html>
<html class="html-hidde-overflow">
   <head>
   	  <meta charset="utf-8" />
      <title><?= $title ?></title>
	  <meta name="description" content="<?= $description ?>">
	  <meta name="keywords" content="<?= $keywords ?>">
	  <meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' />
	  
	  <meta property="og:title" content="<?= $title ?>" />
	  <meta property="og:type" content="<?= $ogType ?>" />
	  <meta property="og:url" content="<?php echo base_url(uri_string()) ?>" />
	  <meta property="og:image" content="<?= base_url().$image ?>" />
	  <meta property="og:description" content="<?= $description ?>" />
	  
	  <link rel="shortcut icon" type="image/png" href="<?php echo base_url() ?>asset/img/favicon.png"> 
	  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/bulma/0.4.2/css/bulma.min.css'>

	  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	  
	  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.0/css/all.css" integrity="sha384-OLYO0LymqQ+uHXELyx93kblK5YIS3B2ZfLGBmsJaUyor7CpMTBsahDHByqSuWW+q" crossorigin="anonymous">



	  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/assets/owl.carousel.min.css'>
	  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
	  

	  <link rel="preconnect" href="https://fonts.googleapis.com">
	  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;300;400;500;600;800;900&display=swap" rel="stylesheet">
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css" />

	  


	  <script>var base_url = "<?php echo base_url() ?>";</script>
	  <?= $_styles ?>
	  
	  <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/main.css?v=<?=time()?>" type="text/css" media="all" />

   </head>
   <body class="html-hidde-overflow">
	  <?= $header ?>        
	  <?= $content ?>
   	  <?= $footer ?>
	  
	  <script src='https://code.jquery.com/jquery-2.2.4.min.js'></script>

	  <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
	  <script src="https://gitcdn.xyz/repo/thesmart/jquery-scrollspy/0.1.3/scrollspy.js"></script>

	  <script defer type="text/javascript" src="<?php echo base_url() ?>asset/js/main.js?v=<?=time()?>"></script> 
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

	 <!-- Latest compiled and minified JavaScript -->
	 <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>

	 <!-- (Optional) Latest compiled and minified JavaScript translation files -->
	 <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/i18n/defaults-*.min.js"></script>
	  
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
      <script src='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/owl.carousel.min.js?v=4'></script>
	  <?= $_scripts ?>

	  <script type="text/javascript">
	  	$('#slideproduct').owlCarousel({
	  	  loop: true,
	  	  margin: 10,
	  	  nav: true,
	  	  autoplay: false,
	  	  autoplayHoverPause: false,
	  	  autoHeight: true,
	  	  responsive: {
	  	    0: {
	  	      items: 1,
              autoHeight:true
	  	    },
	  	    600: {

	  	      items: 1,
              autoHeight:true
	  	    },
	  	    1000: {
	  	      items: 1,
              autoHeight:true
	  	    }
	  	  }
	  	});

	  </script>

      <script type="text/javascript">
          $(".form-send").submit(function(event){
	  		event.preventDefault(); //prevent default action 
	  		var post_url = $(this).attr("action"); //get form action url
	  		var request_method = $(this).attr("method"); //get form GET/POST method
	  		var form_data = $(this).serialize(); //Encode form elements for submission
	  		
	  		$.ajax({
	  			url : post_url,
	  			type: request_method,
	  			data : form_data,
	  			beforeSend: function() {
	  		        // setting a timeout
	  		        $('.enviar').addClass('disable');
	  		        $('.load-gif').show('slow');
	  		        //$('.message').hide("slow");
	  		    },
	  		    success: function(response){
	  		        window.location.replace("<?php echo base_url()?>home/gracias/");
	  		    }
	  		});
	  		});

      </script>

   </body>
</html>